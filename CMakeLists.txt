cmake_minimum_required(VERSION 3.12)

project(zeromq-hello-world LANGUAGES CXX)

# Organize targets into folders.
set_property(GLOBAL PROPERTY USE_FOLDERS ON)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

# Extend a list of directories specifying a search path for CMake modules
# to be loaded by the the include() or find_package() commands.
list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")

include(conan)

#
# Dependencies
#

conan_cmake_run(CONANFILE conanfile.txt BASIC_SETUP CMAKE_TARGETS BUILD missing)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()

add_subdirectory(extern/libzmq)

set(ZEROMQ_LIB_DIR ${CMAKE_CURRENT_BINARY_DIR}/extern/libzmq CACHE PATH "Provide submodule with proper directory")
set(ZEROMQ_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/extern/libzmq/include CACHE PATH "Provide submodule with proper directory")
set(ZMQPP_LIBZMQ_CMAKE true CACHE BOOL "Tell submodule to search in cmake project")
add_subdirectory(extern/zmqpp)
add_subdirectory(modules)
